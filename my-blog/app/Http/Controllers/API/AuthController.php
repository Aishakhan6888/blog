<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class AuthController extends Controller
{

    public function register(Request $request)
    {
        // $validatedData = $request->validate([
        //     'name' => 'required|max:55',
        //     'email' => 'email|required|unique:users',
        //     'password' => 'required|confirmed'
        // ]);

        $data = $request->all();

        $data['password'] = bcrypt($request->password);

        $user = User::create($data);
        if($user){
            return response()->json([
                'status' => true,
                'data' => $user
            ]);
        }
        return response()->json([
            'status' => false,
            'data' => []
        ]);
    }

    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);
        if (!auth()->attempt($loginData)) {
            return response(['message' => 'Invalid Credentials']);
        }

        $loggedUser = Auth::user();
     $user = [
         'id' => $loggedUser->id,
         'name' => $loggedUser->name,
         'email'=> $loggedUser->email,
         'token' => $loggedUser->createToken('MyApp')->plainTextToken,
     ];

        return response(['status' => true,'data'=>$user]);

    }

    //
}
