<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Book;

class UserController extends Controller
{
    public function getUser(Request $request)
    {
    $user= Book::all();
    return response()->json([
        'status'=>true,
        'data'=> $user
    ]);
    }
    //
    public function create(Request $request) {
      $input = $request->all();
      $validator = Validator::make($input, [
      'name' => 'required',
      'author' => 'required'
      ]);
      if($validator->fails()){
      return $this->sendError('Validation Error.', $validator->errors());       
      }
      $book = Book::create($input);
  
        return response()->json([
          "success" => true,
          "message" => "book created successfully.",
          "data" => $book
          ]);
      }
    
      public function update(Request $request, $id) {
        $input = $request->all();
$validator = Validator::make($input, [
'name' => 'required',
'author' => 'required'
]);
if($validator->fails()){
return $this->sendError('Validation Error.', $validator->errors());       
}
$book->name = $input['name'];
$book->detail = $input['author'];
$book->save();
return response()->json([
"success" => true,
"message" => "book updated successfully.",
"data" => $book
]);

      }

      public function destroy($id)
    {
        //
        $book->delete();
        return response()->json([
        "success" => true,
        "message" => "book deleted successfully.",
        "data" => $book
        ]);
    }
   
    public function show($id)
    {
        //
        $book = Book::find($id);
        if (is_null($book)) {
        return $this->sendError('Product not found.');
        }
        return response()->json([
        "success" => true,
        "message" => "Product retrieved successfully.",
        "data" => $book
        ]);
        
    }
   
       
}
