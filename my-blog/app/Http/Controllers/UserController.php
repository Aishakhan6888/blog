<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Account;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Student;
use App\Models\Skill;
use App\Models\Employee;
use App\Models\Position;
use App\Models\Experience;
use App\Models\Book;
use \NumberFormatter;

use Illuminate\Database\Eloquent\Builder;


// class Currency
// {
    
//     function __construct(string $name)
//     {
//         $this->name = $name;
//     }
// }


class UserController extends Controller
{

    public function index() 
    {
        // $posts = Employee::has('skills', '<=', 3)->get();      //1
        // dd($posts);
        // $users = Employee::whereHas('skills', function($q){     //2
        //     $q->where('created_at', '>=', '2021-08-01 00:00:00');
        // })->get();
        // $users = Employee::with('skills')->get();                //3
        // $posts = Employee::whereHas('skills', function($q){      //4
        //     $q->where('skill', '=', 'php');
        // })->get();

    //     $posts = Employee::whereHas('skills', function ($query) {   //5
    //         $query->where('skill', 'like', 'php%');
    // })->with(['skills' => function ($query) {
    //         $query->where('skill', 'like', 'php%');
    // }])->get();
    // $posts = Employee::find(1)->skills;    //6
    // $posts = Employee::find(1)->skills()->where('skill', 'php')->first(); //7
//     $posts = Employee::with('skills')
//  ->whereHas('skills', function (Builder $query) {                       //8
//  $query->where('skill', 'like', 'PHP%');
//  })
 //->get();
//  $posts = Employee::whereRelation(
//     'skills', 'created_at', '>=', now()->subHour()                      //9
// )->with('skills')->get();
 
// $posts = Employee::whereRelation(
//     'skills', 'created_at', '>=', now()->subDays()                      //10
// )->with('skills')->get();
 

// $posts = Employee::whereRelation(
//     'skills', 'created_at', '>=', now()->subMonths()                      //11
// )->with('skills')->get();

// $posts = Employee::whereHas('skills', function($query) {                   //12
//     $query->where('skill','php')->where('name','aisha');
// })->with('skills')->get();

    //  dd($posts);
###########Avg()##############################
  $avg = Book::avg('price');
  $avg = Book::min('price');
  $avg = Book::max('price');
  $avg = Book::sum('price');
//  dd($avg);
################# chunck() ######################
// $users = Book::chunk(3, function ($book) {
//     foreach ($book as $chunck){
//         //  echo $chunck;
//     }
//     echo"<br><br>";
// });
// exit();

#################Collect()#########################
 $book = Book::all();
$ex = $book->where('id','1');
// dd($ex);
//  $sk = $book->collect();
// echo $sk ;
// exit();
##############Count#################################
$com = Book::count('id');
// dd($com);
##############concat()#################################
 $user = BOOK::all();
 $concat = $user->concat([
    'name' => 'aisha',
     'surname' => 'khan'
 ]);
//  dd($concat);
##############contains()#################################
$us = BOOK::first();
$user =collect($us)->contains('abvc');
//   dd($user);
##############countBy()#################################
$by = Book::all();
$emp = $by->countBy('id');
$emp = $by->countBy('price');
$emp = $by->countBy('email');

//  dd($emp->all());
##############crossJoin()#################################
$us = BOOK::first('id');
$matrix =collect($us)->crossJoin(['p','q']);
// dd($matrix);
##############diff()#################################
$books = Book::all();   
$users = $books->diff(Book::whereIn('id', [1, 2, 3])->get());
//  dd($users);
 ##############diffAssoc()#################################
 $assoc = $books->diffAssoc(Book::whereIn('id', [1, 2, 3])->get());
// dd($assoc);
 ##############diffKeys()#################################
 $key = $books->diffKeys(Book::whereIn('id', [1, 2, 3])->get());
//  dd($key);

#########################except()########################
$except = $books->except([1, 2, 3]);
// dd($except);

#########################duplicates()########################
 $dup = Book::all('email');
 $duplicate = $dup->duplicates();
//  dd($duplicate);
 #########################each()########################
//  $books = Book::all();   
// $books->each(function($user){
//     if($user->id==80){
//         return false;
//     }
//     print($user->id);
//     echo"<br>";
// });
// exit();
 #########################filter()########################
$filter =$books->filter()->all();
// dd($filter);
 #########################map()########################
// return $books->map(function ($user){
//     return strtoupper($user->name);
// });
// exit();
 #########################mapInfo()########################
 $collection = Book::all();
// $map = $collection->mapInto(Currency::class);
// $collection = collect(['USD', 'EUR', 'GBP']);

// $currencies = $collection->mapInto(Book::class);

// dd($currencies->all());
// dd($map->all());
 #########################mapSpread()########################
//  $collect = Book::all();

//  $chunks = $collect->chunk(2);

//  $sequence = $chunks->mapSpread(function ($value) {
//     return $value->name;
//  });
 
//  exit();
 #########################merge()########################
//  $merge1 =Book::all();
//  $merge2 =Skill::all();

//  $merged = $merge->merge($merge2);

//  dd($merged->toArray());

// $test = 1000025.05;

// $f = new \NumberFormatter( locale_get_default(), \NumberFormatter::SPELLOUT );

// $word = $f->format($test);

//  dd($word);

function convertToIndianCurrency($number) {
    $no = round($number);
    $decimal = round($number - ($no = floor($number)), 2) * 100;    
    $digits_length = strlen($no);    
    $i = 0;
    $str = array();
    $words = array(
        0 => '',
        1 => 'One',
        2 => 'Two',
        3 => 'Three',
        4 => 'Four',
        5 => 'Five',
        6 => 'Six',
        7 => 'Seven',
        8 => 'Eight',
        9 => 'Nine',
        10 => 'Ten',
        11 => 'Eleven',
        12 => 'Twelve',
        13 => 'Thirteen',
        14 => 'Fourteen',
        15 => 'Fifteen',
        16 => 'Sixteen',
        17 => 'Seventeen',
        18 => 'Eighteen',
        19 => 'Nineteen',
        20 => 'Twenty',
        30 => 'Thirty',
        40 => 'Forty',
        50 => 'Fifty',
        60 => 'Sixty',
        70 => 'Seventy',
        80 => 'Eighty',
        90 => 'Ninety');
    $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;            
            $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural;
        } else {
            $str [] = null;
        }  
    }
    
    $Rupees = implode(' ', array_reverse($str));
    $paise = ($decimal) ? "And Paise " . ($words[$decimal - $decimal%10]) ." " .($words[$decimal%10])  : '';
    return ($Rupees ? 'Rupees ' . $Rupees : '') . $paise . " Only";
}
 
// echo "56721351.61 = " . convertToIndianCurrency(56721351.61);



return view('index');        
    }
    











    public function getUsers(Request $request)
    {
        $employee = Employee::all();
        return \DataTables::of($employee)
            ->addColumn('actions', function($employee) {
                return '<a href='.\URL::route('edit-users', $employee->id).' class="btn btn-success btn-sm">Edit</a>
                    <a href='.\URL::route('show-users', $employee->id).' class="btn btn-info btn-sm">View</a>
                    <a href='.\URL('destroy', $employee->id).' class="btn btn-danger btn-sm">Delete</a>';

                })
            ->rawColumns(['actions'])
            ->make(true);
    }




    public function show($id)
    {
        $user = Employee::with('skills')->find($id);

        $employe_skills = [];
        foreach(explode(',',$user->skills->position) as $bu){
            if(isset($bu)){
                $skus = Position::where('id',$bu)->first();
                if(!empty($skus)){
                 array_push($employe_skills,$skus->position);
                }  
            }
        }
        $employe_experience = [];
        foreach(explode(',',$user->skills->experience) as $bu){
            if(isset($bu)){
                $skus = Experience::where('id',$bu)->first();
                if(!empty($skus)){
                 array_push($employe_experience,$skus->experience);
                }  
            }
        }

        return view('show', compact('user','employe_skills','employe_experience'));

    }

    
    public function skill() 
    {
        $position = Position::all();
        $experience = Experience::all();

        return view('skill', compact('position', 'experience'));
    }
    
    

    public function store(Request $request)
    {
        $validated = $request->validate([
            'skill' => 'required',
            'position' => 'required',
            'experience' => 'required',


        ]);
     
       $employee =  Employee::create(['name'=> $request->name]);
  

        if($employee->id){
            foreach($request->skill as $key=>$skill){
                $data = new Skill();
                $data->user_id = $employee->id;
                $data->skill = $skill;
                $data->position = implode(',',$request->position);
                $data->experience =  implode(',',$request->experience);
                $data->save();

            
                
            }
        }
        return back()->with('success', 'New Data has been added.');
    }


    public function edit($id)
    {
        $data = Employee::with('skills')->find($id);
     
        $position = Position::all();
        $experience = Experience::all();


        return view('edit', compact('data','position','experience'));
        }
    
    public function update(Request $request, $id)
    {

        $validated = $request->validate([
            'skill' => 'required',
            'position' => 'required',
            'experience' => 'required',


        ]);
           $employee = Employee::find($id);
           $employee->name = $request->name;
           $employee->update();
           if($employee->id){
               $skillDelete = Skill::where('user_id',$employee->id)->delete();
               if($skillDelete){
                   foreach($request->skill as $key=>$skill){
                       $data = new Skill();
                       $data->user_id = $employee->id;
                       $data->skill = $skill;
                       $data->position = implode(',',$request->position);
                       $data->experience =  implode(',',$request->experience);
                       $data->save();
                     }; 
                    }
                }
        
        return back()->with('success', 'Data has been Updated.');
    }

    public function destroy($id)
    {
        $employee =Employee::where('id',$id)->first();
        $employeeDelete  = $employee->delete();
        if($employeeDelete){
            Skill::where('user_id',$employee->id)->delete();
            
        }

        
        return back()->with('success', 'Data has been Deleted.');

    }

    public function insertData() 
    {
        $position = Position::all();

        return view('insertdata', compact('position'));
    }
    
    public function storeData(Request $request)
    {
        $validated = $request->validate([
            'skill' => 'required',
            'position_id' => 'required',


        ]);
        $input = $request->all();
        $input['position_id'] = implode(",",$request->position_id);
        $data = Student::create($input);
    }
    public function showData()
    {
      
        $user = Student::with('getPosition')->get();
    
        $bus = [];
        foreach(explode(',',$user->position_id) as $bu){
            if(isset($bu)){
                dd($bu);
                $position = Position::where('id',$bu)->first();
                dd($position);
                if(!empty($bu->getPosition->position)){
                    dd($bu->getPosition->position);
                 array_push($bus,$bu->getPosition->position);
                }
                
            }
        return view('showData', compact('user'));

    }


    // public function mySearch(Request $request)
    // {
    // 	if($request->has('search')){
    // 		$users = Employee::search($request->get('name'))->get();	
    // 	}else{
    // 		$users = Employee::get();
    // 	}


    // 	return view('my-search', compact('users'));
    // }
    }
}
