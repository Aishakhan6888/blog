<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'skill','position','experience','user_id'
    ];
    
    public function users()
    {
        return $this->belongsTo(Employee::class,'user_id','id');
    }
    public function getPosition()
    {
        return $this->belongsTo(Position::class,'position','id');
    }
}
