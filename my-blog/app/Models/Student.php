<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $fillable = [
        'skill','position_id'
    ];
    
    public function getPosition()
    {
        return $this->belongsTo(Position::class,'position_id','id');
    }
}
