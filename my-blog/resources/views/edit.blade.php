@extends('layout')
@section('content')

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<div class="container">  
@if (Session::has('success'))
            <div class="alert alert-success text-center">
                <p>{{ Session::get('success') }}</p>
            </div>
            @endif 
            <td><a  class="btn btn-success btn-sm" href = "{{ url('index/') }}">Back</a></td>
    <h6 style="text-align:center;" > Employee Skills  </h6><br>  
    <div class="form-group">  
         <form name="add_name" id="add_name" method="POST" action="{{ url('update/'.$data->id) }}">  
         @csrf
         <div class="row">
         <div class="col-sm-3">
<h6> Employee Name</h6> 
</div>

    <div class="col-sm-3">
         <input type="text" name="name" placeholder="Name" class="form-control name_list" style="font-size:15px;margin-bottom:30px;height:30px;" value="{{ $data->name }}"/>
</div>
<div class="col-sm-3">
<button type="button" name="add" id="add" class="btn btn-success btn-sm" style="font-size:15px;">Add</button>
</div>
</div>
         <div class="table-responsive"> 
              
         
                   <table class="table table-bordered" id="dynamic_field"> 
                   @if($data->skills) 
                        <tr>  
                            
                            <td><input type="text" name="skill[]" placeholder="Skills" class="form-control name_list" style="font-size:15px;height:30px;" value="{{ $data->skills->skill }}"/></td> 
                            <td> 
<select input type="text" class="form-control"  id="multiselect1" name="position[]" placeholder=" Blood Group" required style="font-size:15px;" multiple="multiple">
@foreach($position as $item)
<option value="{{ $item->id }}"
@foreach(explode(',',$data->skills->position) as $list){{ ($list) == $item->id ? 'selected' : '' }}
@endforeach>{{ $item->position }}</option>
@endforeach
</select>
</td> 
<td>
<select input type="text" class="form-control" name="experience[]" id="multiselect2" placeholder=" Blood Group" required style="font-size:15px;" multiple="multiple">
@foreach($experience as $item)
<option value="{{ $item->id }}"
@foreach(explode(',',$data->skills->position) as $list){{ ($list) == $item->id ? 'selected' : '' }}
@endforeach>{{ $item->experience }}</option>
@endforeach
</select>
</td>  
 <td><button type="button" class="btn btn-danger deleteRow btn-sm">X</button></td>

@endif
 
                   </table>  
                   <input type="submit" name="btn3" id="submit" class="btn btn-info btn-sm" value="Submit" style="font-size:15px;" />  
              </div>  
         </form>  
    </div>  
</div>  



<!-- statics end -->



    </tbody>
  </table>
  <script type="text/javascript">
        $(document).ready(function(){  
      var i=1;  
      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="skill[]" style="font-size:15px;" placeholder="Skills"class="form-control name_list"/></td><td> <select input type="text" class="form-control" name="position[]" id="address-1" placeholder=" Blood Group"  style="font-size:15px;" required>@foreach($position as $item)<option value="{{ $item->id }}" >{{ $item->position }}</option>@endforeach</select></td><td> <select input type="text" class="form-control" style="font-size:15px;" name="experience[]" id="address-1" placeholder=" Blood Group" required>@foreach($experience as $year)<option value="{{ $year->id }}">{{ $year->experience }}</option>@endforeach</select></td>                               <td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
      $('#submit').click(function(){            
           $.ajax({  
                url:"name.php",  
                method:"POST",  
                data:$('#add_name').serialize(),  
                success:function(data)  
                {  
                     alert(data);  
                     $('#add_name')[0].reset();  
                }  
           });  
      });  
      $('#multiselect1, #multiselect2').select2();

 });  
 $('.deleteRow').click(function(){
     $(this).parent().parent().remove();
});  
//  function deleteRow(e){
//       alert(e);
//      //  $(e).parent().addClass("yesss");
//  }
    </script>
    <script type="text/javascript">
//         function deleteRow(r) {
//   var i = r.parentNode.parentNode.rowIndex;
//   document.getElementById("dynamic_field").deleteRow(i);
// }

         </script>
  @endsection



 