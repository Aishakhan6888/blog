@extends('layout')
@section('content')
 <div class="container">
<table id="users" class="table table-hover display pb-10">
                                        <thead>
                                            <tr>
                                                <th>S.no</th>
                                                <th>Name</th>
                                          
                                                <th>Action &nbsp;&nbsp;&nbsp;&nbsp;<a  class="btn btn-primary btn-sm" href = "{{ url('skill/') }}" style="">Add</a></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                            <th>S.no</th>
                                                <th>Name</th>
                                          
                                                <th>Action</th>
                                            
                                            </tr>
                                        </tfoot>
                                    </table>
</div>
<script>
$(document).ready(function(){
        getusers();
    });
    function getusers(){ 
  
        $('#users').dataTable().fnDestroy();
        $('#users tbody').empty();
        var table=$('#users').DataTable({
            processing: true,
            serverSide: true,
            
            iDisplayLength:50,
            ajax: {
                url: '{{ url('getusers') }}',
                method: 'GET',
                data: ''
            },
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50,100,"All"]
            ],
            columns: [
                {data: 'id', name: 'id', orderable: false, searchable: false },              
                {data: 'name', name: 'name'},
                {data: 'actions', name: 'actions', orderable: false, searchable: false, "width": "30%"}    
            ],           
            order: [[0, 'asc']], 
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_',
                

            }, 
        }); 
    
    }   
</script>
@endsection