@extends('layout')
@section('content')
<div class="container">  


@if (Session::has('success'))
            <div class="alert alert-success text-center">
                <p>{{ Session::get('success') }}</p>
            </div>
            @endif 
 <form name="add_name" id="add_name" method="POST" action="{{ url('inserData') }}">  
 {!! csrf_field() !!}
 <div class="col-12">
    <label class="visually-hidden" for="inlineFormInputGroupUsername">Username</label>
    <div class="input-group">
      <div class="input-group-text">@</div>
      <input
        type="text"
        class="form-control"
        id="inlineFormInputGroupUsername"
        placeholder="Username"
        name="skill"
      />
    </div>
  </div>

  <div class="col-12">
    <label class="visually-hidden" for="inlineFormSelectPref">Preference</label>
    <select input type="text" class="form-control"  id="multiselect1" name="position_id[]" placeholder=" Blood Group" required style="font-size:15px;" multiple="multiple">
    @foreach($position as $item)
 <option value="{{ $item->id }}" >{{ $item->position }}</option>
                                   @endforeach

    </select>
  </div>

  <div class="col-12">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="inlineFormCheck" />
      <label class="form-check-label" for="inlineFormCheck">
        Remember me
      </label>
    </div>
  </div>

  <div class="col-12">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>
</div>
<script type="text/javascript">
        $(document).ready(function(){  
$('#multiselect1, #multiselect2').select2();
 });  
    </script>
  
  @endsection