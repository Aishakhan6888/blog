@extends('layout')
@section('content')

<body>
<div class="container" id="content">
<td><a  class="btn btn-primary btn-sm" href = "{{ url('index/') }}">Back</a></td>
<a class="btn btn-success btn-sm" href="javascript:window.print()">print</a>


<div class="row">
         <div class="col-sm-3">
<h6 style="margin-top:30px;"> Employee Name</h6>
</div>
    <div class="col-sm-6">
         <input type="text" name="name" placeholder="Name" class="form-control name_list" style="font-size:15px;margin-top:30px;height:30px;" value="{{ $user->name }}" readonly/>
</div></div>
<table border = "1" class="table" style="margin-top:30px;">
<tr>
<td>ID</td>
<td>Skill</td>
<td>Position</td>
<td>Experience</td>


</tr>

@if($user->skills)
<tr>
<td>{{ $user->id }}</td>
<td>{{ $user->skills->skill }}</td>
<td>
@foreach($employe_skills as $skills)
{{$skills.' ,'}}
@endforeach
</td>
<td>
@foreach($employe_experience as $experience)
{{$experience.' ,'}}
@endforeach
</td>
</tr>
@endif
</table>
</div>
</body>
@endsection 
