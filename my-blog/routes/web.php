<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/create',[UserController::class,'create']);
Route::get('/index', [UserController::class, 'index']);
Route::get('getusers', [UserController::class, 'getUsers']);

// Route::get('/index', [UserController::class, 'my-search']);s
Route::get('/show/{id}', [UserController::class, 'show'])->name('show-users');

Route::get('/find',[UserController::class,'find']);
Route::view('multidata','multidata');
Route::post('/skill', [UserController::class, 'store']);
Route::get('/skill', [UserController::class, 'skill']);
Route::get('/inserData', [UserController::class, 'insertData']);
Route::post('/inserData', [UserController::class, 'storeData']);
Route::get('/showData', [UserController::class, 'showData']);
Route::post('/store-input-fields', [UserController::class, 'store']);
Route::get('/set/{id}', [UserController::class, 'edit'])->name('edit-users');
Route::post('/update/{id}', [UserController::class, 'update']);
Route::get('/destroy/{id}', [UserController::class, 'destroy']);

// Route::get('/', function () {
//     return view('welcome');
// });




